CREATE TABLESPACE DATA_DAT DATAFILE 'C:\oraclexe\app\oracle\oradata\xe\DATA_DAT.dbf' SIZE 50m;
ALTER TABLESPACE DATA_DAT ADD DATAFILE 'C:\oraclexe\app\oracle\oradata\xe\DATA_DAT2.dbf' SIZE 50m;

CREATE USER "DATOS" 
DEFAULT TABLESPACE "DATA_DAT" 
TEMPORARY TABLESPACE "TEMP" 
IDENTIFIED BY "Admin12345" 
ACCOUNT UNLOCK;

GRANT "CONNECT" TO "DATOS";
GRANT "RESOURCE" TO "DATOS";
GRANT CREATE TABLE TO DATOS;
GRANT CREATE VIEW TO DATOS;
--******************************************************************************
create table tipos(
id_Tipo int primary key,
nombre varchar(50)
);
create table clientes(
id_cliente int primary key,
nombre varchar(50),
cedula int unique,
telefono int
);
create table equipos(
id_equipo int primary key,
descripcion varchar(100),
fecha_ingreso date,
id_cliente int,
id_tipo int,
CONSTRAINT FK_TIPO_EQUIPOS FOREIGN KEY (id_Tipo) references tipos(id_Tipo),
CONSTRAINT FK_CLIENTE_EQUIPOS FOREIGN KEY (id_cliente) references clientes(id_cliente)
);
create table facturas(
Id_factura int primary key,
Id_equipo int,
Estado char,
Costo float,
CONSTRAINT CK_ESTADO_facturas CHECK (ESTADO IN ('N', 'U'))
CONSTRAINT FK_EQUIPO_FACTURAS FOREIGN KEY (id_equipo) references EQUIPOS(id_equipo)
);
create table Historial_Equipos(
ID_HISTORIAL int primary key,
id_equipo int,
fecha_ingresado date,
CONSTRAINT FK_EQUIPO_HISTORIAL FOREIGN KEY (id_equipo) references EQUIPOS(id_equipo)
);
--******************************************************************************
insert into tipos values(1,'Laptop');
insert into tipos values(2,'Escritorio');
insert into tipos values(3,'Celular');
insert into tipos values(4,'Impresora');
insert into clientes values(1,'Andres',1,1);
insert into clientes values(2,'Pedro',2,1);
insert into clientes values(3,'Melissa',3,1);
insert into clientes values(4,'Sofia',4,1);
insert into clientes values(5,'Julian',5,1);
insert into clientes values(6,'Monserat',6,1);
insert into Equipos values (1,'Liempieza',1,3);
insert into Equipos values (2,'Revision de RAM',2,1);
insert into Equipos values (3,'Mejora',3,2);
insert into Equipos values (4,'Revision de Tinta',4,4);
insert into Equipos values (5,'Liempieza',5,1);
insert into Equipos values (6,'Liempieza',6,1);
insert into Equipos values (7,'Desecho',3,3);
insert into Equipos values (8,'Mantenimiento',6,4);
insert into facturas values(1,1,'U',10323.50);
insert into facturas values(2,7,'U',25000);
insert into facturas values(3,4,'N',15662.10);
insert into facturas values(4,3,'N',11332);
--******************************************************************************
--Obtiene informaci�n simulando un cierre
--V_FACTURAS mostrar� una vista con los atributos de:
--Factura que sera el identificador de la factura
--Nombre refiriendose al nombre del cliente due�o del dispositivo
--Tipo la categoria de dispositivo que se encuentra en el sistema
--Descripcion es informaci�n de que se le debe realizar al dispositivo
--Ingreso la fecha de cuando el equipo fue entregado y registrado
--Precio el costo con el que fue facturado
--Estado si el dispositivo ya fue devuelvo o no su estado sera diferente
--Estado_Equipo evidencia la manera en que fue considerado el dispositivo
CREATE VIEW V_FACTURAS AS
select nvl(f.id_factura,0) factura, c.nombre, t.nombre tipo, e.descripcion, e.fecha_ingreso Ingreso, nvl(f.costo,0) precio,
case
when f.id_factura is null then 'Sin Facturar'
when f.id_factura > 0 then 'Facturado'
end estado,
case
when f.estado='N' then 'Nuevo'
when f.estado='U' then 'Usado'
when f.estado is null then 'En Revision'
end Estado_Equipo
from clientes c
left join equipos e
on c.id_cliente=e.id_cliente
left join tipos t
on e.id_tipo=t.id_tipo
left join facturas f
on f.id_equipo= e.id_equipo;

--Obtiene informaci�n acerca de ganancias
--V_GANANCIAS mostrar� una vista con los atributos de: 
--cantidad que muestra cuantos dispositivos existen de ese tipo
--nombre del dispositivo
--ganancias recolectadas sobre el mismo dispositivo
CREATE VIEW V_GANANCIAS AS
select count(t.id_tipo) cantidad, t.nombre, sum(f.costo) Ganancias
from equipos e
left join clientes c
on e.id_cliente=c.id_cliente
inner join tipos t
on e.id_tipo=t.id_tipo
inner join facturas f
on f.id_equipo=e.id_equipo
group by t.nombre;
--******************************************************************************
--El proposito de la funcion F_GANANCIASMES es darle un numero de mes para ver
--las ganancias totales de el mes indicado
CREATE OR REPLACE FUNCTION F_GANANCIAMES (F_NUMERO_MES IN INT)
RETURN FLOAT IS
GANANCIA FLOAT;
BEGIN
    BEGIN
    select nvl(round(sum(f.costo),2),0) into GANANCIA
    from facturas f
    inner join equipos e
    on f.id_equipo=e.id_equipo
    inner join historial_equipos he
    on he.id_equipo=e.id_equipo
    WHERE extract(month from he.fecha_ingresado)=F_NUMERO_MES;
    EXCEPTION 
    WHEN NO_DATA_FOUND THEN 
        RAISE_APPLICATION_ERROR(-20001,'NO SE ENCONTRARON DATOS'); 
    WHEN TOO_MANY_ROWS THEN     
        RAISE_APPLICATION_ERROR(-20002,'REGISTROS DUPLICADOS'); 
    WHEN OTHERS THEN     
        RAISE_APPLICATION_ERROR(-20003,'SUGIO UN ERROR NO ESPERADO');     
    END; 
RETURN GANANCIA; 
END;
select F_GANANCIAMES(8) from dual;

--El proposito de la funcion F_CANTIDA_ENCARGOS es dar por parametros el codigo 
--de un cliente para que se devuleva la cantidad de encargos que existen
CREATE OR REPLACE FUNCTION F_CANTIDA_ENCARGOS (F_CODIGO IN clientes.id_cliente%TYPE)
RETURN int IS
CANTIDAD int;
BEGIN
    BEGIN
    select nvl(count(e.id_cliente),0) into CANTIDAD
    from equipos e
    inner join clientes c
    on e.id_cliente=c.id_cliente
    WHERE e.id_cliente=F_CODIGO;
    EXCEPTION 
    WHEN NO_DATA_FOUND THEN 
        RAISE_APPLICATION_ERROR(-20001,'NO SE ENCONTRARON DATOS'); 
    WHEN TOO_MANY_ROWS THEN     
        RAISE_APPLICATION_ERROR(-20002,'REGISTROS DUPLICADOS'); 
    WHEN OTHERS THEN     
        RAISE_APPLICATION_ERROR(-20003,'SUGIO UN ERROR NO ESPERADO');     
    END; 
RETURN CANTIDAD; 
END;
select F_CANTIDA_ENCARGOS(6) from dual;

--El proposito de la funcion F_EXISTECLIENTE es dar por parametros el codigo 
--de un cliente para verificar si este cliente ya existe
CREATE OR REPLACE FUNCTION F_EXISTECLIENTE (F_CODIGO IN clientes.id_cliente%TYPE)
RETURN clientes.nombre%TYPE IS
V_EXISTENCIA clientes.nombre%TYPE;
BEGIN
    BEGIN
    select nombre INTO V_EXISTENCIA from clientes where id_cliente=F_CODIGO;
    EXCEPTION 
    WHEN NO_DATA_FOUND THEN 
        RAISE_APPLICATION_ERROR(-20001,'NO EXISTE EL CLIENTE'); 
    WHEN TOO_MANY_ROWS THEN     
        RAISE_APPLICATION_ERROR(-20002,'REGISTROS DUPLICADOS'); 
    WHEN OTHERS THEN     
        RAISE_APPLICATION_ERROR(-20003,'SUGIO UN ERROR NO ESPERADO');     
    END; 
RETURN V_EXISTENCIA; 
END;
select F_EXISTECLIENTE(3) from dual;
--******************************************************************************

--******************************************************************************
--Crea una secuencia que funcionara como identificador autoaumentable para el ingreso
--de id_historial en historial_equipos
create sequence sec_historial
  start with 1
  increment by 1
  maxvalue 99999
  minvalue 1;
--El trigger TR_HISTORIAL_EQUIPOS tiene como proposito almacenar la fecha en que un
--dispositivo ingresa al sistema esto lo har� despues de un insert
CREATE OR REPLACE TRIGGER TR_HISTORIAL_EQUIPOS
AFTER insert
ON EQUIPOS
FOR EACH ROW
BEGIN
    INSERT INTO HISTORial_EQUIPOS(ID_HISTORIAL,ID_EQUIPO,FECHA_INGRESADO)VALUES
    (sec_historial.NEXTVAL,:NEW.ID_EQUIPO,SYSDATE);
END TR_HISTORIAL_EQUIPOS;
select * from Historial_EQUIPOS;
--******************************************************************************

